#!/bin/sh
set -ex

SOURCE=example.org
PORT=22

# rsync flags to use
# The default ones are:
#   -a  archive mode
#   -x  don't cross file system borders
#   -v  verbose
#   -H  copy hard links
FLAGS="-axvH"

# rsync recurses down, but doesn't cross filesystem borders.
# If subfolders are on a different filesystem but should also be synced, include them here
# Include a *trailing slash* so the content of the source directory is copied into the target
DIRECTORIES="/ /var/ /opt/"

for dir in $DIRECTORIES
do
    rsync $FLAGS -e "ssh -p $PORT" --exclude-from=exclude.txt "$SOURCE":"$dir" "$dir"
done
